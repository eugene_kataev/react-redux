import React from "react"

export default class Year extends React.Component {
    constructor(props) {
        super(props)
        this.onBtnClick = this.onBtnClick.bind(this)
    }
    onBtnClick(event) {
        return this.props.setYear(event.target.textContent)
    }

    render() {
        return <div>
            <button onClick={this.onBtnClick}>123</button>
            <button onClick={this.onBtnClick}>12345</button>
            <button onClick={this.onBtnClick}>12345678</button>
            <p>This year has been chosen - {this.props.year}</p>
        </div>
    }
}